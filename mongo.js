//	Activity:

	/*>> Using Robo3T, repopulate our users collections using CRUD operations (Same database: session28)*/

//Create
	/*>> Add at least five of the members of your favorite fictional group or musical band into our users collection with the following fields:

		-firstName -string
		-lastName -string
		-email -string
		-password -string
		-isAdmin -boolean 
			note: initially none of the users are admin.

	>> Add 3 new courses in a new courses collection with the following fields:

		-name - string
		-price - number
		-isActive - boolean 
			note: initially all courses should be inactive

	>> Note: If the collection you are trying to add a document to does not exist yet, mongodb will add it for you.*/
	

//Read

	/*>> Find all regular/non-admin users.*/

//Update

	/*>> Update the first user in the users collection as an admin.
	>> Update one of the courses as active.*/

//Delete

	/*>> Delete all inactive courses*/


//Add all of your query/commands here in mongo.js



//bandMembers collection
db.users.insertMany([
		{
			firstName: "Chito",
			lastName: "Miranda",
			email: "cm@mail.com",
			password: "chito001",
			isAdmin: false 
			
		},
		{
			firstName: "Vinci",
			lastName: "Montaner",
			email: "vm@mail.com",
			password: "vinci001",
			isAdmin: false 
			
		},
		{
			firstName: "Buwi",
			lastName: "Menese",
			email: "bm@mail.com",
			password: "buwi001",
			isAdmin: false 
			
		},
		{
			firstName: "Gab",
			lastName: "Kee",
			email: "gk@mail.com",
			password: "gab001",
			isAdmin: false 
			
		},
		{
			firstName: "Darius",
			lastName: "Semana",
			email: "ds@mail.com",
			password: "darius001",
			isAdmin: false 
			
		},


		])

db.users.find({isAdmin: false})

db.users.updateOne({firstName: "Chito"}, {$set: {isAdmin : true}})



// newCourses collection
db.newCourses.insertMany([
		{
			name: "Music",
			price: 1000,
			isActive: false
		},
		{
			name: "Art",
			price: 1500,
			isActive: false
		},
		{
			name: "Physics",
			price: 2000,
			isActive: false
		}


		])

db.newCourses.updateOne({name: "Art"}, {$set: {isActive : true}})

db.newCourses.deleteMany({isActive: false})